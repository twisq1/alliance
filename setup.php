<?php
session_start();

require_once('php/config.php');
require_once('php/MainMenu.php');
require_once('php/Content.php');
require_once('php/Login.php');
require_once('php/Action.php');
require_once('php/DbFactory.php');
require_once('php/UrlHandler.php');

$docroot = SuperGlobals::getServer("DOCUMENT_ROOT");
$filename = null;
$okcount = 0;
$errorcount = 0;

// Check if there is an action to perform.
$actionName = $_GET['action'];
if ($actionName == 'ExecuteSql') {
	$filename = $_GET['file'];
	
	$dbh = DbFactory::getDefaultHandler();
	$lines = file($docroot . "/sql/" . $filename);
	$templine = '';

	// Loop through each line
	foreach ($lines as $line) {

		// Skip it if it's a comment
		if (substr($line, 0, 2) == '--' || $line == '')
			continue;

		// Add this line to the current segment
		$templine .= $line;

		// If it has a semicolon at the end, it's the end of the query
		if (substr(trim($line), -1, 1) == ';') {

			// Perform the query
			$query = new DbQuery($templine);
			if ($dbh->executeQuery($query)) {
				$okcount++;
			} else {
				$errorcount++;
			}
			$dbh->freeQuery();
			$templine = '';
		}
	}
}

?>

<!DOCTYPE html>
<html lang="nl">
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<head>
		<title>Voetbaltoernooi Manager Setup</title>
		<link rel="stylesheet" type="text/css" href="css/main.css">
	</head>

	<body>

		<header>
			<div class="logo col-2">
				<img class="logo-img" src="<?php print(IMG_BASEDIR . IMG_LOGO); ?>"/>
			
			<!-- Do not put whitespace between these DIVs! -->
			</div><div class="title col-10">
				TWISQ TOERNOOI MANAGER

			</div>
		</header>

		<div class="content col-12">
			<?php

				// Get files from sql directory and filter everything out that has not sql in their name.
				$filelist = ["createtables.sql", "user.sql"];/*array_filter(scandir($docroot . "/sql"), function ($filename) {
					return str_contains($filename, 'sql'); 
				});*/

				$html = '<div class="content">';

				if ($filename) {
					$html .= '<p>';
					$html .= "Filename: $filename<br/>";
					$html .= "Succesvol uitgevoerde queries: $okcount<br/>";
					$html .= "Foutief uitgevoerde queries: $errorcount<br/>";
					$html .= '</p>';
				}

				$html .= 'Deze bestanden staan klaar om verwerkt te worden:';
				$html .= '<ul>';
				$me = SuperGlobals::getMe();
				foreach($filelist as $filename) {
					$html .= "<li><a href='$me?action=ExecuteSql&file=$filename'>$filename<a></li>";
				}
				$html .= '<li><a href="/index.php">Ga naar index.php</a></li>';
				$html .= '</ul>';
				$html .= '</div>';
				print($html);
			?>
		</div>

		<footer>
			<a id="twisq" title="De maker van deze site" href="https://www.twisq.nl">www.twisq.nl</a>
		</footer>

	</body>
</html>
<?php DbFactory::closeDatabase(); ?>
