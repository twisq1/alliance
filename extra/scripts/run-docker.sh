#!/bin/bash

# Make sure that we are in the directory where the script is located.
cd "$(dirname "$0")"

# First run deploy script.
./deploy.sh

# Build Docker
docker build -t alliance .

# Run Docker
docker run -dp 80:80 -v ${PWD}/../../deploy:/app alliance
