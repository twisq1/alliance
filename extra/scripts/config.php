<?php

error_reporting( E_ALL | E_STRICT);

define('LOC_LIBFILES','php');

require_once('php/MainMenuItem.php');
$mainMenuDefinition = array(
	new MainMenuItem(10, 'VANDAAG', 'Overzicht van poules en standen van vandaag', MMI_DEFAULT),
	new MainMenuItem(12, 'VRIJDAG', 'Overzicht van poules en standen op vrijdag', MMI_DEFAULT),
	new MainMenuItem(15, 'ZATERDAG', 'Overzicht van poules en standen op zaterdag', MMI_DEFAULT),
	new MainMenuItem(20, 'ZONDAG', 'Overzicht van poules en standen op zondag', MMI_DEFAULT),
	new MainMenuItem(30, 'ALLE WEDSTRIJDEN', 'Alle wedstrijden', MMI_DEFAULT),
    new MainMenuItem(40, 'DIASHOW', 'Toon alle poules achtereenvolgens', MMI_DEFAULT),
	new MainMenuItem(50, 'ADMIN', 'Bewerk het wedstrijdschema', MMI_DEFAULT),
	new MainMenuItem(100, 'LOGIN', 'Login', MMI_LOGIN)
);

// Define images
define('IMG_BASEDIR', 'img/');
define('IMG_HELP', 'help.png');
define('IMG_LOGO', 'alliance.gif');

date_default_timezone_set('Europe/Amsterdam');

define('DATABASE_MAKE','Mysql');
define('DATABASE_HOST','localhost');
define('DATABASE_USER','{{ db_user }}');
define('DATABASE_PASSWORD','{{ db_password }}');
define('DATABASE_NAME','{{ db_name }}');

?>
