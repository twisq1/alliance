#!/usr/bin/python

import sys
import csv
from optparse import OptionParser

class Csv2Sql:

    def __init__(self):
        parser = OptionParser()
        parser.add_option("-i", "--infile", dest="infile", help="csv file to read")
	parser.add_option("-t", "--table", dest="tablename", help="name of sql table")
        (self.options, self.args) = parser.parse_args()

    def run(self):
        try:
            csvfile = open(self.options.infile, 'rb')
        except:
            print "Cannot read file %s. Try '%s -h' for help." % (self.options.infile, sys.argv[0])
            return
        data = csv.reader(csvfile, delimiter=',')
	header = next(data)
	sql_base = "INSERT INTO `" + self.options.tablename + "` (`" + header[0] + "`"
	for field in header[1:]:
		sql_base += ", `" + field + "`"
	sql_base += ") VALUES ('"
	for record in data:
		sql = sql_base + record[0] + "'"
		for value in record[1:]:
			sql += ", '" + value.replace("'", "\\'") + "'"
		sql += ");"
		print sql


if __name__ == "__main__":
    c2s = Csv2Sql()
    c2s.run()

