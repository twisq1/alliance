--
-- Database: `twisq_alliance`
--

-- --------------------------------------------------------

--
-- Table structure for table `match`
--

CREATE TABLE IF NOT EXISTS `match` (
  `id` int(11) NOT NULL auto_increment,
  `home_id` int(11) NOT NULL DEFAULT '0',
  `away_id` int(11) NOT NULL DEFAULT '0',
  `home_goals` int(11) DEFAULT NULL,
  `away_goals` int(11) DEFAULT NULL,
  `field` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time` time DEFAULT NULL,
  `referee` varchar(80) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `poule`
--

CREATE TABLE IF NOT EXISTS `poule` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(80) collate utf8_unicode_ci NOT NULL default '',
  `date` date NOT NULL default '2017-01-01',
  `diashow` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `team`
--

CREATE TABLE IF NOT EXISTS `team` (
  `id` int(11) NOT NULL auto_increment,
  `poule_id` int(11) NOT NULL default '0',
  `name` varchar(80) collate utf8_unicode_ci NOT NULL default '',
  `played` int(11) NOT NULL default '0',
  `points` int(11) NOT NULL default '0',
  `win` int(11) NOT NULL default '0',
  `equal` int(11) NOT NULL default '0',
  `loose` int(11) NOT NULL default '0',
  `goals` int(11) NOT NULL default '0',
  `against` int(11) NOT NULL default '0',
  `rank` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Tabel structuur voor tabel `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(10) NOT NULL auto_increment,
  `username` varchar(100) collate utf8_unicode_ci NOT NULL,
  `md5` varchar(32) collate utf8_unicode_ci NOT NULL COMMENT 'md5 hash of password',
  `full_name` varchar(255) collate utf8_unicode_ci NOT NULL,
  `email` varchar(255) collate utf8_unicode_ci default NULL,
  `level` int(10) NOT NULL default '0' COMMENT 'Hoe ver gaan de bevoegdheden',
  `change_allowed` smallint(6) NOT NULL default '1',
  PRIMARY KEY  (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Users and passwords in md5' AUTO_INCREMENT=1 ;

