--
-- Gegevens worden uitgevoerd voor tabel `user`
-- 

REPLACE INTO `user` (`user_id`, `username`, `md5`, `full_name`, `email`, `level`, `change_allowed`) VALUES
(1, 'paul', MD5("{{ paul_password }}"), 'Paul', '', 3, 0),
(2, 'robin', MD5("{{ robin_password }}"), 'Robin', '', 3, 0),
(3, 'peter', MD5("{{ robin_password }}"), 'Peter', '', 3, 0);
