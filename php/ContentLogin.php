<?php

require_once('php/config.php');
require_once('php/SuperGlobals.php');
require_once('php/Content.php');
require_once('php/MainMenu.php');
require_once('php/Link.php');

class ContentLogin extends Content
{
	protected $page;

	public function setPage($page)
	{
		$this->page = $page;
	}

	public function checkPermission()
	{
		return true;
	}

	public function getContent()
	{
		$html = $this->getLogContent();
		return $html;
	}

	protected function getLogContent()
	{
		$mmid = $this->mainMenu->searchItem(MMI_LOGIN)->getId();
		$html = '<form method="post" action="' . SuperGlobals::getMe() . /*'?mm=' . $mmid . */ '">';
		if ($this->login->getUserId()) {
			$html .= 'Je bent succesvol ingelogd als ' . $this->login->getFullName() . '.<br/>';
			$html .= '<p>Klik op onderstaande knop om uit te loggen.</p>';
			$html .= '<input type="hidden" name="username" value="logoff"></input>';
			$html .= '<input type="hidden" name="password" value="logoff"></input>';
			$html .= '<table><tr>';
			$html .= '<td class="form">';
			$html .= '<input class="button" name="but_login" type="submit" value="Log uit"></input>';
			$html .= '</td></tr></table>';
			$html .= '</form>';
			return $html;
		} else {
			$username = $this->login->getUsername();
			if (($username == 'logoff') || ($username == false)) {
				$html .= 'Je bent momenteel niet ingelogd.<br/>';
			} else {
				$html .= 'De opgegeven combinatie van gebruikersnaam en wachtwoord is ongeldig.<br/>';
			}
		}
		$html .= '<p>Vul hieronder je gebruikersnaam en wachtwoord in om in te loggen</p>';
		$html .= '<table><tr>';
		$html .= '<td class="form">Gebruikersnaam</td><td class="form"><input name="username"></input></td>';
		$html .= '</tr><tr>';
		$html .= '<td class="form">Wachtwoord</td><td class="form"><input type="password" name="password"></input></td>';
		$html .= '</tr>';
		$html .= '<tr><td class="form">&nbsp;</td><td class="form">';
		$html .= '<input class="button" name="but_login" type="submit" value="Login"></input>';
		$html .= '</td></tr></table>';
		$html .= '</form>';
		return $html;
	}

}

?>
