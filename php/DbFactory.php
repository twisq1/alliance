<?php

/****
 * This file contains a class with a factory method for 'manufacturing' the appropriate database handler
 *
 ***/

/****
 * Use DbFactory to create an appropriate Database Handler, or
 * return an existing handler, and
 * to close it when done
 *
 * It is not possible to make two different database connections at the same time yet
 ***/
class DbFactory
{
	private static $_currentDbHandler = false;	// The last opened database handler, if any
	private static $_referenceCount = 0;		// Number of returned references
	
	/****
	 * createDbHandler
	 * return an instance of a database handler for the given database make.
	 * or return false and report an error
	 * 
	 * parameters: $databaseMake can be Mysql or Oracle for example. Case is irrelevant.
	 *           : $databaseHost location of the database server
	 *           : $databaseUser username whomay login to database
	 *           : $databasePassword password of user
	 *           : $databaseName Database name to use
	 * returns   : An instance of a DbHandler, or
	 *             false if there is no valid subclass available
	 * errors    : reported to error handler
	 ***/
	public static function createDbHandler($databaseMake, $databaseHost, $databaseUser, $databasePassword, $databaseName)
	{
		$databaseMake = strtolower($databaseMake);
		
		// First check if the database handler already exists
		if (self::$_currentDbHandler) {
			$dbHandle = self::$_currentDbHandler;
			if ($dbHandle->getConnectionParameters($currentMake, $currentHost, $currentName)) {
			
				// Once a dbHandler is created, no other connections can be made
				if (($databaseMake == $currentMake) && ($databaseHost == $currentHost) &&
					($databaseName == $currentName)) {
					
					// Database handler is already available. Increase reference count and return handle
					self::$_referenceCount++;
					return $dbHandle;
				} else {
					trigger_error("Impossible to create different database connections", E_USER_ERROR);
					return false;
				}
			}
		}

		// Include right implementation and instantiate a database handler
		$filename = LOC_LIBFILES . '/' . ucfirst($databaseMake) . "DbHandler.php";
		$classname = ucfirst($databaseMake) . "DbHandler";

		if (file_exists($filename)) {
			include_once($filename);
			$dbHandle = new $classname;
			if (!$dbHandle->connect($databaseHost, $databaseUser, $databasePassword, $databaseName))
			{
				return false;
			}
			self::$_currentDbHandler = $dbHandle;
			self::$_referenceCount++;
			return $dbHandle;
		} else {
			trigger_error("Internal error: File '$filename' not found.", E_USER_ERROR);
		}
		return false;
	}


	/**
	 * Connect to database and return handle.
	 */
	public static function getDefaultHandler()
	{
		return self::createDbHandler(DATABASE_MAKE, DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME);
	}
		
	/****
	 * closeDbHandler
	 * decrease reference count and close database connection if there is no reference left
	 *
	 * parameters: $handle to close. This handle must be created by createDbHandler()
	 * returns   : false on error
	 ***/
	public static function closeDbHandler($handle)
	{
		// Check if handle is created by our factory
		if ($handle != self::$_currentDbHandler) {
			trigger_error("Cannot close invalid handle",E_USER_ERROR);
			return false;
		}

		self::$_referenceCount--;

		if (!self::$_referenceCount) {
			$handle->close();			// Close only the last reference
			self::$_currentDbHandler = false;
		}
		
		return true;
	}


	/****
	 * closeDbForced
	 * close connection to database if open.
	 ***/
	public static function closeDatabase()
	{
		if (self::$_referenceCount) {
			self::$_currentDbHandler->close();
			self::$_currentDbHandler = false;
			self::$_referenceCount = 0;
		}
	}


	/****
	 * getReferenceCount
	 * get number of references in use, for example to check if all references are properly closed
	 *
	 * parameters: none
	 * returns   : number of references
	 ***/
	public static function getReferenceCount()
	{
		return self::$_referenceCount;
	}
	
}

?>
