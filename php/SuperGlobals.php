<?php

  class SuperGlobals
  {
    static function getRequest($name)
    {
    	print("Warning: getRequest is depricated. Use another get-method");
      if (isset($_REQUEST[$name])) {
        $value = $_REQUEST[$name];
      } else {
        $value = false;
      }
      return $value;
    }

    static function getPost($name)
    {
      if (isset($_POST[$name])) {
        $value = $_POST[$name];
      } else {
        $value = false;
      }
      return $value;
    }

    static function getGet($name)
    {
    	print("getGet is depricated. Use UrlHandler instead");
      if (isset($_GET[$name])) {
        $value = $_GET[$name];
      } else {
        $value = false;
      }
      return $value;
    }

    static function getSession($name)
    {
      if (isset($_SESSION[$name])) {
        $value = $_SESSION[$name];
      } else {
        $value = false;
      }
      return $value;
    }

    static function getServer($name)
    {
      if (isset($_SERVER[$name])) {
        $value = $_SERVER[$name];
      } else {
        $value = false;
      }
      return $value;
    }

    static function getMe()
    {
      return self::getServer('PHP_SELF');
    }

    static function setSession($name, $value)
    {
      $_SESSION[$name] = $value;
    }

  }

?>
