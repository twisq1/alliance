<?php

require_once('php/config.php');
require_once('php/Action.php');
require_once('php/SuperGlobals.php');
require_once('php/DbFactory.php');
require_once('php/MainMenu.php');
require_once('php/TournamentManager.php');

class ActionEditMatch extends Action
{
	/****
	 * This function is responsible for checking if the user has permissions to do this action
	 *
	 * parameters: none
	 * returns   : true if ok
	 ***/
	protected function checkPermission()
	{
		if ($this->login->getLevel() > 0) {
			return true;
		}
		return false;
	}


	/****
	 * execute - do it!
	 * returns   : nothing
	 ***/
	protected function execute()
	{
    // Get values
    $match = SuperGlobals::getPost('match');
    $home = SuperGlobals::getPost('home');
    $away = SuperGlobals::getPost('away');
    $field = SuperGlobals::getPost('field');
    $time = SuperGlobals::getPost('time');
	$referee = SuperGlobals::getPost('referee');
    $delete = SuperGlobals::getPost('delete');
    if ($delete) {
      $home = 'NULL';
      $away = 'NULL';
    }
    
	if (!is_numeric($home)) $home = 'NULL';
	if (!is_numeric($away)) $away = 'NULL';

    // Update database
		$dbh = DbFactory::getDefaultHandler();
		$query = new DbQuery("UPDATE `match` SET `home_goals` = :home, `away_goals` = :away, `field` = ':field', `time` = ':time', `referee` = ':referee'  WHERE `id` = ':match'");
		$query->bindParam('match', $match);
		$query->bindParam('home', $home);
    	$query->bindParam('away', $away);
    	$query->bindParam('field', $field);
    	$query->bindParam('time', $time);
		$query->bindParam('referee', $referee);
		$dbh->executeQuery($query);
		$dbh->freeQuery();
    
    // Now update points
    $helper = new TournamentManager();
    $helper->updatePoints($match);
	}
}

?>
