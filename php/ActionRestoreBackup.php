<?php

require_once('php/config.php');
require_once('php/Action.php');
require_once('php/SuperGlobals.php');
require_once('php/DbFactory.php');
require_once('php/MainMenu.php');
require_once('php/TournamentManager.php');

class ActionRestoreBackup extends Action
{
    private $poules;
    private $teams;
    private $matches;

	/****
	 * This function is responsible for checking if the user has permissions to do this action
	 *
	 * parameters: none
	 * returns   : true if ok
	 ***/
	protected function checkPermission()
	{
		if ($this->login->getLevel() > 1) {
			return true;
		}
		return false;
	}


	/****
	 * execute - do it!
	 * returns   : nothing
	 ***/
	protected function execute()
	{
        if(isset($_POST["submit"])) {
            $data = array();
            ini_set('auto_detect_line_endings',TRUE);
            $file = fopen($_FILES['fileToUpload']['tmp_name'],"r");
            if (!feof($file)) {
                $header = trim(fgets($file));
                if (($header === 'poule,datum,diashow,team1,team2,score1,score2,veld,tijd,scheidsrechter') ||
                    ($header === '"poule","datum","diashow","team1","team2","score1","score2","veld","tijd","scheidsrechter"')) {

                    $linenr = 0;
                    $oklines = 0;
                    while(! feof($file))
                    {
                        $linenr++;
                        $record = array();
                        $csv = fgetcsv($file, 0, ',' );
                        if ($csv === false) {
                            $this->error("Error in CSV line $linenr.");
                        }
                        if (count($csv) == 10) {
                            $record['poule'] = $csv[0];
                            $record['date'] = $csv[1];
                            $record['show'] = $csv[2];
                            $record['team1'] = $csv[3];
                            $record['team2'] = $csv[4];
                            $record['score1'] = $csv[5];
                            $record['score2'] = $csv[6];
                            $record['field'] = $csv[7];
                            $record['time'] = $csv[8];
                            $record['referee'] = $csv[9];
                            array_push($data, $record);
                            $oklines++;
                        }
                    }
                    fclose($file);
                    //$this->error("OK-lines = $oklines.");

                    $result = $this->test($data);
                    if ($result === true) {
                        $this->clearDatabase();
                        $this->loadDatabase($data);
                        header('Location: ' . SuperGlobals::getme());
                        exit();
                    } else {
                        $this->error($result);
                    }
                    exit();
                } else {
                    $this->error('Ongeldig bestand, onjuiste header.');
                }
            }
        }
	}

    protected function test($data)
    {
        $this->poules = array();
        $this->teams = array();
        $this->matches = array();

        // Get poules from data
        $poule_id = 1;
        foreach($data as $record) {
            if (!array_key_exists($record['poule'], $this->poules)) {
                $poule = array();
                $poule['id'] = $poule_id++;
                $poule['name'] = $record['poule'];
                $poule['date'] = $record['date'];
                $poule['diashow'] = $record['show'];
                $this->poules[$record['poule']] = $poule;
            } else {
                if ($this->poules[$record['poule']]['date'] != $record['date']) {
                    return 'Fout in poule ' . $record['poule'] . '. Niet alle datums komen overeen.';
                }
                if ($this->poules[$record['poule']]['diashow'] != $record['show']) {
                    return 'Fout in poule ' . $record['poule'] . '. Niet alle diashows komen overeen.';
                }
            }
        }

        // Get teams from data
        $team_id = 1;
        foreach($data as $record) {

            // Home team
            if (!array_key_exists($record['team1'], $this->teams)) {
                $team = array();
                $team['id'] = $team_id++;
                $team['name'] = $record['team1'];
                $team['poule_id'] = $this->poules[$record['poule']]['id'];
                $this->teams[$record['team1']] = $team;
            } else {
                if ($this->teams[$record['team1']]['poule_id'] != $this->poules[$record['poule']]['id']) {
                    return 'Fout met team ' . $record['team1'] . '. Het komt voor in meerdere poules.';
                }
            }

            // Away team
            if (!array_key_exists($record['team2'], $this->teams)) {
                $team = array();
                $team['id'] = $team_id++;
                $team['name'] = $record['team2'];
                $team['poule_id'] = $this->poules[$record['poule']]['id'];
                $this->teams[$record['team2']] = $team;
            } else {
                if ($this->teams[$record['team2']]['poule_id'] != $this->poules[$record['poule']]['id']) {
                    return 'Fout met team ' . $record['team2'] . '. Het komt voor in meerdere poules.';
                }
            }
        }

        // Get matches from data
        $match_id = 1;
        foreach($data as $record) {
            $match = array();
            $match['id'] = $match_id++;
            $match['home_id'] = $this->teams[$record['team1']]['id'];
            $match['away_id'] = $this->teams[$record['team2']]['id'];
            $match['home_goals'] = $record['score1'];
            $match['away_goals'] = $record['score2'];
            $match['field'] = $record['field'];
            $match['time'] = $record['time'];
            $match['referee'] = $record['referee'];
            array_push($this->matches, $match);
        }

        return true;
    }

    protected function clearDatabase()
    {
		$dbh = DbFactory::getDefaultHandler();
		$query = new DbQuery("TRUNCATE `match`");
		$dbh->executeQuery($query);
		$dbh->freeQuery();
		$query = new DbQuery("TRUNCATE `team`");
		$dbh->executeQuery($query);
		$dbh->freeQuery();
		$query = new DbQuery("TRUNCATE `poule`");
		$dbh->executeQuery($query);
		$dbh->freeQuery();
    }

    protected function loadDatabase($data)
    {
        $dbh = DbFactory::getDefaultHandler();

        // Fill poules
        foreach($this->poules as $poule) {
            $query = new DbQuery("INSERT INTO `poule` (`id`, `name`, `date`, `diashow`) VALUES (':id', ':name', ':date', ':diashow')");
            $query->bindParam('id', $poule['id']);
            $query->bindParam('name', $poule['name']);
            $query->bindParam('date', $poule['date']);
            $query->bindParam('diashow', $poule['diashow']);
            $dbh->executeQuery($query);
            $dbh->freeQuery();
        }

        // Fill teams
        foreach($this->teams as $team) {
            $query = new DbQuery("INSERT INTO `team` (`id`, `poule_id`, `name`) VALUES (':id', ':poule_id', ':name')");
            $query->bindParam('id', $team['id']);
            $query->bindParam('poule_id', $team['poule_id']);
            $query->bindParam('name', $team['name']);
            $dbh->executeQuery($query);
            $dbh->freeQuery();
        }

        // Fill matches
        foreach($this->matches as $match) {
            $home_goals = $match['home_goals'];
            if (!is_numeric($home_goals)) $home_goals = 'NULL';
            $away_goals = $match['away_goals'];
            if (!is_numeric($away_goals)) $away_goals = 'NULL';
            $query = new DbQuery("INSERT INTO `match` (`home_id`, `away_id`, `home_goals`, `away_goals`, `field`, `time`, `referee`) VALUES (':home_id', ':away_id', :home_goals, :away_goals, ':field', ':time', ':referee')");
            $query->bindParam('home_id', $match['home_id']);
            $query->bindParam('away_id', $match['away_id']);
            $query->bindParam('home_goals', $home_goals);
            $query->bindParam('away_goals', $away_goals);
            $query->bindParam('field', $match['field']);
            $query->bindParam('time', $match['time']);
            $query->bindParam('referee', $match['referee']);
            $dbh->executeQuery($query);
            $dbh->freeQuery();
        }

        // Now update points
        $helper = new TournamentManager();
        $helper->updatePoints($match);

    }

    protected function error($message) {
        print($message);
        exit();
    }
}

?>
