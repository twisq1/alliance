<?php

  require_once('php/config.php');
  require_once('php/DbFactory.php');
  require_once('php/Logger.php');
  require_once('php/SuperGlobals.php');

  class Login
  {
    protected $username;
    protected $md5;
    protected $user_id;
    protected $new_login;
    protected $email;
    protected $level;
	protected $full_name;
	protected $change_allowed;

	const MAX_LEVEL = 3;
	
	static public function getLevelDescription($level)
	{
		switch($level) {
			case 1:
				return '1 - Gebruiker kan afgeschermde pagina\'s bekijken.';
			case 2:
				return '2 - Gebruiker kan pagina\'s bewerken.';
			case 3:
				return '3 - Supergebruiker.';
		}
	}
	
    // The constructor collects login information.
    function __construct()
    {
      $this->username = false;
      $this->md5 = false;
      $this->user_id = false;
      $this->new_login = false;
      $this->change_allowed = false;
      $this->level = 0;

      // If new data exists, use it.
      $this->username = SuperGlobals::getPost('username');
      if ($this->username) {
        $this->new_login = true;
        SuperGlobals::setSession('username', $this->username);
      } else {
        $this->username = SuperGlobals::getSession('username');
      }

      $password = SuperGlobals::getPost('password');
      if ($password) {
        $this->md5 = md5($password);
        SuperGlobals::setSession('md5', $this->md5);
      } else {
        $this->md5 = SuperGlobals::getSession('md5');
      }

    }
    
    // Check if login username and password do match.
    public function check()
    {
    	$this->user_id = false;
    	$record = $this->getUser($this->username, $this->md5);
    	if ($record) {
	        $this->user_id = $record['user_id'];
	        $this->level = $record['level'];
	        $this->email = $record['email'];
		    $this->full_name = $record['full_name'];
		    $this->change_allowed = $record['change_allowed'];    		
    	}
    	return $this->user_id;
    }
    
    // Check given password.
    public function checkPassword($hash)
    {
    	if ($this->getUser($this->username, $hash)) return true;
    	return false;
    }
    
    // Lookup a user by its username and password.
    protected function getUser($username, $hash)
    {
      $dbh = DbFactory::getDefaultHandler();
      $query = new DbQuery("SELECT * FROM user WHERE username=':username' AND md5=':md5'");
      $query->bindParam('username',$username);
      $query->bindParam('md5',$hash);
      $dbh->executeQuery($query);
      if (!$dbh->getRecord($record)) {
      	$record = false;
      }
      $dbh->freeQuery();
      return $record;
    }

    function getUserId()
    {
      return $this->user_id;
    }

	function getLevel()
	{
	  return $this->level;
	}

	function getEmail()
	{
	  return $this->email;
	}

	function getUsername()
	{
		return $this->username;
	}

	function getFullName()
	{
		return $this->full_name;
	}
	
	public function changePasswordAllowed()
	{
		return $this->change_allowed;
	}
	
	// Return true if current user is allowed to edit a page
	public function editAllowed()
	{
		if ($this->level > 1) {
			return true;
		}
		return false;
	}
	
	// Return true if current user is allowed to see a protected page with given protection level. 
	public function showAllowed($page_level)
	{
		// Non protected pages are always visible.
		if ($page_level == 0) return true;

		// Normal protected pages are visible for users who are logged in.
		if (($page_level == 1) && ($this->level >= 1)) return true;

		// Special protected pages are visibla for administrators.
		if (($page_level == 2) && ($this->level >= 2)) return true;

		// Otherwise, page is invisible.
		return false;
	}
	
	// Am I allowed to edit user?
	public function editUserAllowed()
	{
		// Administrators and higher.
		if ($this->level >= 2) {
			return true;
		}
		return false;
	}

	// Am I allowed to add a new user?	
	public function addUserAllowed()
	{
		// Ony superusers are.
		if ($this->level >= 3) {
			return true;
		}
		return false;
	}
	
  }

?>
