<?php

require_once('php/config.php');
require_once('php/SuperGlobals.php');
require_once('php/DbFactory.php');
require_once('php/Login.php');
require_once('php/UrlHandler.php');

class MainMenu
{
	protected $mmId;
	private $menuDefinition;

	function __construct($menuDefinition)
	{
        $this->menuDefinition = $menuDefinition;
		$url = UrlHandler::getInstance();
		$mm = $url->getParameter('mm');
        if ($mm) {
		  $this->mmId = $mm;
        } else {
          	$menuItem = $this->searchItem(MMI_DEFAULT);
			if ($menuItem) {
				$this->mmId = $menuItem->getId();
			} else {
				$this->mmId = 0;
			}
        }   
	}

	// Search for the first menu item with specified flag set.
	public function searchItem($flag)
	{
		foreach($this->menuDefinition as $menuItem)
		{
			if ($menuItem->checkFlag($flag)) {
				return $menuItem;
			}
		}
		return null;
	}

	// Check if mmId is valid.
	public function isIdValid()
	{
		foreach ($this->menuDefinition as $menuItem) {
			if ($menuItem->getId() == $this->mmId) {
				return true;
			}
		}
		return false;
	}

	public function show()
	{
		print('<ul class="menu">');
		foreach ($this->menuDefinition as $menuItem) {
			$this->showItem($menuItem);
		}
		print('</ul>');
	}

	protected function showItem(MainMenuItem $item)
	{
		if ($this->mmId == $item->getId()) {
			$class = 'mm_sel';
		} else {
			$class = 'mm_unsel';
		}
		print("<li class=\"menu\"><a class='$class' href='?mm=" . $item->getId() . "' title='" . $item->getDescription() ."'>");
		print($item->getName());
		print('</a></li>');
	}

	public function getMainMenuId()
	{
		return $this->mmId;
	}

}

?>
