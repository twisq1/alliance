<?php

require_once('php/config.php');

abstract class ContentFrame
{
	protected $content;
	protected $login;

	public static function InstantiateContentFrame(Content $content, Login $login, $frameName)
	{
		$classname = 'ContentFrame' . $frameName;
		$classpath = LOC_LIBFILES . '/' . $classname . '.php';
		if (!file_exists($classpath)) {
			trigger_error('Invalid content frame', E_USER_ERROR);
			return false;
		}

		require_once($classpath);
		$contentFrameObject = new $classname;

		$contentFrameObject->initialize($content, $login);
		return $contentFrameObject;
	}

	protected function initialize(Content $content, Login $login)
	{
		$this->content = $content;
		$this->login = $login;
	}

	public abstract function getContent();

}

?>
