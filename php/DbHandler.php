<?php

/****
 * This database handler defines a generic interface for database operations.
 * The implementation of this interface is done in a concrete class, for example MysqlDbHandlerClass.
 *
 * To use this database handler, first invoke DbFactory::createDbHandler($databaseMake) and pass the make of the database you want to use.
 *
 * Internal errors are reported to the default error handler and can be trapped by your user error handler.
 *
 ***/

require_once('DbQuery.php');

/****
 * The interface defines all functions which are implemented in one of the concrete database handler classes
 ***/
interface DbHandler
{
	public function getConnectionParameters(&$make, &$host, &$database);// Get the parameters of this connection
	public function executeQuery(DbQuery $query);						// Query the database, use a DbQuery object to prepare the query
	public function freeQuery();										// Free last query to be ready for the next one
	public function getRecord(&$record);								// Fetch one row of results of last query as an associative array
	public function getNumRows();										// Get the number of rows in the last result
	public function getAffectedRows();									// Get the number of rows which are affected by the last query
	public function getInsertedId();									// Get the id created by autoincrement at the last INSERT query
	public function gotoFirstResult();									// When fetching results, use this function to go back to the first result
	
	// Do not use these methods directly. Use DbFactory instead.
	public function connect($host, $username, $password, $database);	// Connect to given database
	public function close();											// Close connection
}

?>
