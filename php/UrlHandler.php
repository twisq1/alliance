<?php

require_once('php/DbFactory.php');

class UrlHandler
{
	static private $instance = null;
	protected $parameters;
	private $parentId = 0;		// Opgehaalde parentId uit database

	private function __construct() {
		$this->parameters = $_GET;
	}
	
	static public function getInstance() {
		if (self::$instance == null) {
			self::$instance = new UrlHandler();
		}
		return self::$instance;
	}
	
	public function getAllParameters() {
		return $this->parameters;
	}

	public function getParameter($name) {
		if (isset($this->parameters[$name])) {
			return $this->parameters[$name];
		}
		return false;
	}
	
	public function setParameter($name, $value) {
		$this->parameters[$name] = $value;
	}

	public function removeParameter($name) {
		unset($this->parameters[$name]);
	}
	
	// If parameter content is given, read school_id, menu_id and parent_id from database
	public function checkContent() {
		$content = $this->getParameter('content');
		if ($content) {
			$dbh = DbFactory::getDefaultHandler();
			$query = new DbQuery("SELECT menu_id, parent_id FROM content WHERE content_id = ':content_id'");
			$query->bindParam('content_id', $content);
			$dbh->executeQuery($query);
			if ($dbh->getRecord($record)) {
				$this->setParameter('mm', $record['menu_id']);
				$this->parentId = $record['parent_id'];
			}
			$dbh->freeQuery();
		}
		
	}
	
	public function getParentId()
	{
		return $this->parentId;
	}
}

?>
