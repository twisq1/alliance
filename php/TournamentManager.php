<?php

require_once('php/config.php');
require_once('php/DbFactory.php');

// vim: ts=2 sw=2 et

class TournamentManager
{
  /**
  * Get Poules on a given date or all dates if date is false.
  **/
  public function getPoules($date)
  {
    $dbh = DbFactory::getDefaultHandler();
    if ($date != false) {
      $query = new DbQuery("SELECT * FROM `poule` WHERE `date` = ':date' ORDER BY `id`");
      $query->bindParam('date', $date);
    } else {
      $query = new DbQuery("SELECT * FROM `poule` ORDER BY `id`");
    }
    $dbh->executeQuery($query);
    $results = array();
    while ($dbh->getRecord($record)) {
      array_push($results, $record);
    }
    $dbh->freeQuery();
    return $results;
  }

  /**
  * Get a single poules with a given pouleid
  **/
  public function getPoule($pouleid)
  {
    $dbh = DbFactory::getDefaultHandler();
    $query = new DbQuery("SELECT * FROM `poule` WHERE `id` = ':pouleid'");
    $query->bindParam('pouleid', $pouleid);
    $dbh->executeQuery($query);
    $dbh->getRecord($poule);
    $dbh->freeQuery();
    return $poule;
  }

  /**
  * Get teams within a given poule.
  **/
  public function getTeams($pouleid)
  {
    $dbh = DbFactory::getDefaultHandler();
    if ($pouleid === false) {
      $query = new DbQuery("SELECT * FROM `team`");
    } else {
      $query = new DbQuery("SELECT * FROM `team` WHERE `poule_id` = ':pouleid' ORDER BY `rank`");
      $query->bindParam('pouleid', $pouleid);
    }
    $dbh->executeQuery($query);
    $results = array();
    while ($dbh->getRecord($record)) {
      array_push($results, $record);
    }
    $dbh->freeQuery();
    return $results;
  }

  /**
  * Get matches within a given poule or date
  **/
  public function getMatches($pouleid, $date)
  {
    $dbh = DbFactory::getDefaultHandler();
    $querystring = "
      SELECT `match`.`id`, `t1`.`name` AS `home_team`, `t2`.`name` AS `away_team`, `home_id`, `away_id`, `home_goals`, `away_goals`, `field`, DATE_FORMAT(`time`, '%H:%i') AS time, `referee`
      FROM `match` 
      JOIN `team` t1 ON `match`.`home_id` = `t1`.`id`
      JOIN `team` t2 ON `match`.`away_id` = `t2`.`id`
      JOIN `poule` ON `t1`.`poule_id` = `poule`.`id`
    ";
    if ($pouleid) {
      $querystring .= " WHERE `poule`.`id` = $pouleid";
    }
    if ($date) {
      if ($pouleid) $querystring .= " AND"; else $querystring .= " WHEN";
      $querystring .= "`date` = '$date'";
    }
    $querystring .= " ORDER BY `date`, `time`, `match`.`id`";
  
    $query = new DbQuery($querystring);
    $dbh->executeQuery($query);
    $results = array();
    while ($dbh->getRecord($record)) {
      $onematch = array();
      $onematch['hometeam'] = $record['home_team'];
      $onematch['awayteam'] = $record['away_team'];
      $onematch['home_id'] = $record['home_id'];
      $onematch['away_id'] = $record['away_id'];
      $onematch['homescore'] = $record['home_goals'];
      $onematch['awayscore'] = $record['away_goals'];
      $onematch['field'] = $record['field'];
      $onematch['time'] = $record['time'];
      $onematch['id'] = $record['id'];
      $onematch['referee'] = $record['referee'];
      array_push($results, $onematch);
    }
    $dbh->freeQuery();
    return $results;
  }

  /**
  * Create an array with a backup of the whole database
  **/
  public function getBackup()
  {
    $dbh = DbFactory::getDefaultHandler();
    $querystring = "
      SELECT `poule`.`name`, `poule`.`date`, `poule`.`diashow`, `t1`.`name` AS `home_team`, `t2`.`name` AS `away_team`, `home_goals`, `away_goals`, `field`, DATE_FORMAT(`time`, '%H:%i') AS time, `match`.`referee`
      FROM `match` 
      JOIN `team` t1 ON `match`.`home_id` = `t1`.`id`
      JOIN `team` t2 ON `match`.`away_id` = `t2`.`id`
      JOIN `poule` ON `t1`.`poule_id` = `poule`.`id`
    ";
  
    $query = new DbQuery($querystring);
    $dbh->executeQuery($query);
    $results = array();
    while ($dbh->getRecord($record)) {
      array_push($results, $record);
    }
    $dbh->freeQuery();
    return $results;
  }

  /**
  * Get a single match
  **/
  public function getMatch($match_id)
  {
    $dbh = DbFactory::getDefaultHandler();
    $query = new DbQuery("
      SELECT `match`.`id`, `t1`.`name` AS `home_team`, `t2`.`name` AS `away_team`, `home_id`, `away_id`, `home_goals`, `away_goals`, `field`, DATE_FORMAT(`time`, '%H:%i') AS time, `referee`
      FROM `match` 
      JOIN `team` t1 ON `match`.`home_id` = `t1`.`id`
      JOIN `team` t2 ON `match`.`away_id` = `t2`.`id`
      WHERE `match`.`id` = ':match_id'"
    );
    $query->bindParam('match_id', $match_id);
    $dbh->executeQuery($query);
    $onematch = array();
    while ($dbh->getRecord($record)) {
      $onematch['hometeam'] = $record['home_team'];
      $onematch['awayteam'] = $record['away_team'];
      $onematch['home_id'] = $record['home_id'];
      $onematch['away_id'] = $record['away_id'];
      $onematch['homescore'] = $record['home_goals'];
      $onematch['awayscore'] = $record['away_goals'];
      $onematch['field'] = $record['field'];
      $onematch['time'] = $record['time'];
      $onematch['referee'] = $record['referee'];
      $onematch['id'] = $record['id'];
    }
    $dbh->freeQuery();
    return $onematch;
  }

  /**
  * Get the poule id given a match id
  **/
  public function getPouleId($match_id)
  {
    $dbh = DbFactory::getDefaultHandler();
    $query = new DbQuery("SELECT `poule_id` FROM `team` JOIN `match` ON `match`.`home_id` = `team`.`id` WHERE `match`.`id` = ':match_id'");
    $query->bindParam('match_id', $match_id);
    $dbh->executeQuery($query);
    if ($dbh->getRecord($record)) {
      $poule_id = $record['poule_id'];
    } else {
      $poule_id = false;
    }
    $dbh->freeQuery();
    return $poule_id;
  }

  /**
  * Write teams to database
  **/
  public function setTeams($teams)
  {
    $dbh = DbFactory::getDefaultHandler();
    foreach($teams as $team)
    {
      $query = new DbQuery("UPDATE `team` SET `played` = ':played', `points` = ':points', `win` = ':win', `equal` = ':equal', `loose` = ':loose', `goals` = ':goals', `against` = ':against', `rank` = ':rank' WHERE `id` = ':team_id'");
      $query->bindParam('played', $team['played']);
      $query->bindParam('points', $team['points']);
      $query->bindParam('win', $team['win']);
      $query->bindParam('equal', $team['equal']);
      $query->bindParam('loose', $team['loose']);
      $query->bindParam('goals', $team['goals']);
      $query->bindParam('against', $team['against']);
      $query->bindParam('rank', $team['rank']);
      $query->bindParam('team_id', $team['id']);
      $dbh->executeQuery($query);
      $dbh->freeQuery();
    }
  }
  
  public function updatePoints($match_id)
  {
    // Get poule id
    $poule_id = $this->getPouleId($match_id);
    
    // Get teams
    $teams = $this->getTeams($poule_id);
    
    // First reset all values and fill lookup table
    $newteams = array();
    for($i = 0 ; $i < count($teams) ; $i++) {
      $team = $teams[$i];
      $team['played'] = 0;
      $team['points'] = 0;
      $team['win'] = 0;
      $team['equal'] = 0;
      $team['loose'] = 0;
      $team['goals'] = 0;
      $team['against'] = 0;
      $team['rank'] = 0;
      $newteams[$team['id']] = $team;
    }

    // Get matches
    $matches = $this->getMatches($poule_id, false);
    foreach($matches as $match) {
      if (is_numeric($match['homescore'])) {
        
        // Update played
        $newteams[$match['home_id']]['played']++;
        $newteams[$match['away_id']]['played']++;
        
        // Update points and win/equal/loose
        if ($match['homescore'] > $match['awayscore']) {
          $newteams[$match['home_id']]['points']+=3;
          $newteams[$match['home_id']]['win']++;
          $newteams[$match['away_id']]['loose']++;
        } else if ($match['homescore'] < $match['awayscore']) {
          $newteams[$match['away_id']]['points']+=3;
          $newteams[$match['away_id']]['win']++;
          $newteams[$match['home_id']]['loose']++;
        } else {
          $newteams[$match['home_id']]['points']+=1;
          $newteams[$match['away_id']]['points']+=1;
          $newteams[$match['away_id']]['equal']++;
          $newteams[$match['home_id']]['equal']++;
        }          
        
        // Update score
        $newteams[$match['home_id']]['goals'] += $match['homescore'];
        $newteams[$match['home_id']]['against'] += $match['awayscore'];
        $newteams[$match['away_id']]['goals'] += $match['awayscore'];
        $newteams[$match['away_id']]['against'] += $match['homescore'];
      }
    }

    // sort
    usort($newteams, "compareTeams");
    $rank = 1;
    $keys = array_keys($newteams);
    foreach($keys as $key) {
      $newteams[$key]['rank'] = $rank;
      $rank++;
    }

    // Write to database
    $this->setTeams($newteams);
  }

  public function getPlayedMatches()
  {
    $dbh = DbFactory::getDefaultHandler();
    $query = new DbQuery("SELECT COUNT(*) AS played FROM `match` WHERE `home_goals` IS NOT NULL;");
    $dbh->executeQuery($query);
    if ($dbh->getRecord($record)) {
      $played = $record['played'];
    } else {
      $played = 0;
    }
    $dbh->freeQuery();
    return $played;
  }

  public function getScoredTotal()
  {
    $dbh = DbFactory::getDefaultHandler();
    $query = new DbQuery("SELECT SUM(`home_goals`) AS home, SUM(`away_goals`) AS away FROM `match`;");
    $dbh->executeQuery($query);
    if ($dbh->getRecord($record)) {
      $home = $record['home'];
      $away = $record['away'];
    } else {
      $home = 0;
      $away = 0;
    }
    $dbh->freeQuery();
    return $home + $away;
  }

  public function getChampions()
  {
    $dbh = DbFactory::getDefaultHandler();
    $query = new DbQuery("SELECT `name` AS `champion` FROM `team` WHERE `played` = 3 AND `rank` = 1 ORDER BY `champion`;");
    $dbh->executeQuery($query);
    $results = array();
    while ($dbh->getRecord($record)) {
      array_push($results, $record['champion']);
    }
    $dbh->freeQuery();
    return $results;
  }

  public function getTopscorers()
  {
    $dbh = DbFactory::getDefaultHandler();
    $query = new DbQuery("SELECT `name`,`goals`,`poule_id` FROM `team` ORDER BY `goals` DESC, `name`;");
    $dbh->executeQuery($query);
    $results = array();
    $oneteam = array();
    while ($dbh->getRecord($record)) {
      $oneteam['name'] = $record['name'];
      $oneteam['goals'] = $record['goals'];
      $oneteam['poule_id'] = $record['poule_id'];
      array_push($results, $oneteam);
    }
    $dbh->freeQuery();
    return $results;
  }

}

function compareTeams($team1, $team2) {
  // First test on points
  $compare_points = $team2['points'] - $team1['points'];
  if ($compare_points != 0) return $compare_points;
  
  // Then compare on played
  $compare_played = $team1['played'] - $team2['played'];
  if ($compare_played != 0) return $compare_played;
  
  // Then compare on goals
  $goals1 = $team1['goals'] - $team1['against'];
  $goals2 = $team2['goals'] - $team2['against'];
  $compare_goals = $goals2 - $goals1;
  if ($compare_goals != 0) return $compare_goals;
 
  // Then compare on goals scored
  $compare_home_goals = $team2['goals'] - $team1['goals'];
  if ($compare_home_goals != 0) return $compare_home_goals;
 
  // TODO: Sort on mutual result
  
  // Finally sort on alphabet
  return strcmp($team1['name'], $team2['name']);
}



?>
