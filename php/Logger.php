<?php

  require_once('php/config.php');
  require_once('php/DbFactory.php');

  class Logger
  {
    static function addMessage($user_id, $event, $extra_data=null)
    {
      $ip = $_SERVER['REMOTE_ADDR'];
      $dbh = DbFactory::createDbHandler(DATABASE_MAKE, DATABASE_HOST, DATABASE_USER, DATABASE_PASSWORD, DATABASE_NAME);
      $query = new DbQuery("INSERT INTO log (user_id, ip, event, extra_data, datetime) VALUES (':user_id', ':ip', ':event', ':extra_data', CURRENT_TIMESTAMP())");
      $query->bindParam('user_id', $user_id);
      $query->bindParam('event', $event);
      $query->bindParam('ip', $ip);
      $query->bindParam('extra_data', $extra_data);
      $dbh->executeQuery($query);
      $dbh->freeQuery();
      DbFactory::closeDbHandler($dbh);
    }
  }

?>
