<?php

// vim: ts=2 sw=2 et

require_once('php/Content.php');
require_once('php/Login.php');
require_once('php/UrlHandler.php');
require_once('php/TournamentManager.php');

class ContentDefault extends Content
{
	protected $contentId = 0;					// Current to show
	protected $className = null;
	private $parentId;
  private $tmgr;
  
  function __construct()
  {
    $this->tmgr = new TournamentManager();
  }

	public function getContent()
	{
		$url = UrlHandler::getInstance();
    $mm = $url->getParameter('mm');
    if (!$mm) $mm = 10;
    $today = date("Y-m-d");
    switch($mm) {
      case 10: 
        return $this->getContentOverview($today);
      case 12:
      // TODO: Read hardcoded dates from database!
        return $this->getContentOverview('2023-05-26');
      case 15:
        return $this->getContentOverview('2023-05-27');
      case 20: 
        return $this->getContentOverview('2023-05-28');
      case 30:
        return $this->getContentMatches();
      case 40: 
        return $this->getContentOverview(false);
      case 50:
        return $this->getContentAdmin();
    }
  }

  private function getContentMatches()
  {
		$url = UrlHandler::getInstance();
    if ($this->login->getLevel() > 0) {
      $match = $url->getParameter('match');
      if ($match) return $this->getContentEditMatch($match);
      $admin = true;
    }
    else $admin = false;
		$url = UrlHandler::getInstance();
    $date = $url->getParameter('date');
    if ($date) {
    	$html = '<h1>Alle wedstrijden op ' . $date . '.</h1>';
    } else {
      $html = '<h1>Alle wedstrijden</h1>';
    }
    $matches = $this->tmgr->getMatches(false, $date);
    if (count($matches) == 0) {
      $html .= 'Geen wedstrijden.';
    } else {
      $html .= '<div class="matches">';
      $html .= '<table>';
      $html .= '<thead>';
      $html .= '<th class="field">Veld</th>';
      $html .= '<th class="time">Tijd</th>';
      $html .= '<th class="match" colspan="2">Wedstrijd</th>';
      $html .= '<th class="score" colspan="2">Uitslag</th>';
      $html .= '<th class="referee">Scheidsrechter</th>';
      if ($admin) $html .= '<th class="edit">Invullen</th>';
      $html .= '</thead>';
      $html .= '<tbody>';
      foreach ($matches as $match) {
        $html .= '<tr class="match">';
        $html .= '<td class="field">' . $match['field'] . '</td>';
        $html .= '<td class="time">' . $match['time'] . '</td>';
        $html .= '<td class="hometeam">' . $match['hometeam'] . '</td>';
        $html .= '<td class="awayteam"> - ' . $match['awayteam'] . '</td>';
        $html .= '<td class="homescore">' . $match['homescore'] . '</td>';
        $html .= '<td class="awayscore"> - ' . $match['awayscore'] . '</td>';
        $html .= '<td class="referee">' . $match['referee'] . '</td>';
        if ($admin) $html .= '<td class="edit"><a href="?mm=30&match=' . $match['id'] . '">Bewerken...</a></td>';
        $html .= '</tr>';
      }
      $html .= '</tbody>';
      $html .= '</table>';
      $html .= '</div>';
    }
    return $html;
  }

  private function getContentAdmin()
  {
    $html = '';
    if ($this->login->getLevel() > 1) {
      $html .= '<h1>BEWERK WEDSTRIJDSCHEMA</h1>';
      $html .= '<p style="color:red">LET OP! Door op onderstaande links te klikken kan alle data uit de database verloren gaan. Ga alleen verder als je zeker weet dat dit de bedoeling is.</p>';
      $html .= '<p>';
      $html .= '<hr/>';
      $html .= '<h2>Download Backup</h2>';
      $html .= '<a href="' . SuperGlobals::getme() . '?mm=50&action=GetBackup">Download backup van alle wedstrijden.</a>';
      $html .= '<hr/>';
      $html .= '<h2>Upload Backup</h2>';
      $html .= '
        <form action="' . SuperGlobals::getme() . '?mm=50&action=RestoreBackup" method="post" enctype="multipart/form-data">
        Kies hier de backup die je wilt laden.
        <input type="file" name="fileToUpload" id="fileToUpload"><br/>
        <input type="submit" value="Upload backup" name="submit">
        </form>';
      $html .= '<hr/>';
      $html .= '</p>';
    
    } else {
      $html .= '<p>Je hebt geen rechten om deze pagina te mogen zien. Log in als gebruiker met voldoende rechten.</p>';
    }
    return $html;
  }

  private function getContentEditMatch($match_id)
  {
    $match = $this->tmgr->getMatch($match_id);
    $html = '<h1>BEWERKEN / UITSLAG INVULLEN</h1>';
    $html .= SuperGlobals::getPost('home_goals');
    $html .= '<form method="post" action="' . SuperGlobals::getme() . '?mm=30&action=EditMatch">';
    $html .= '<table><tbody>';
    $html .= '<tr><td>' . $match['hometeam'] . '</td><td><input type="text" name="home" size="10" value="' . $match['homescore'] . '"/></td></tr>';
    $html .= '<tr><td>' . $match['awayteam'] . '</td><td><input type="text" name="away" size="10" value="' . $match['awayscore'] . '"/></td></tr>';
    $html .= '<tr><td colspan="2">&nbsp;</td></tr>';
    $html .= '<tr><td>Veld</td><td><input type="text"domain name="field" size="10" value="' . $match['field'] . '"/></td></tr>';
    $html .= '<tr><td>Tijd</td><td><input type="text"domain name="time" size="10" value="' . $match['time'] . '"/></td></tr>';
    $html .= '<tr><td>Scheidsrechter</td><td><input type="text"domain name="referee" size="10" value="' . $match['referee'] . '"/></td></tr>';
    $html .= '<tr><td colspan="2">&nbsp;</td></tr>';
    $html .= '<tr><td>&nbsp;</td><td>';
    $html .= '<input type="hidden" name="match" value="' . $match_id . '"/>';
    $html .= '<input class="button" name="ok" type="submit" value="Ok"/>&nbsp;';
    $html .= '<input class="button" name="delete" type="submit" value="Uitslag verwijderen"/>&nbsp;';
    $html .= '</td></tr>';
    $html .= '</tbody></table>';
    $html .= '</form>';
    return $html;
  }

  private function getContentOverview($date_parameter)
  {          
    $html = "";
		$url = UrlHandler::getInstance();
    $pouleid = $url->getParameter('poule');
    $date = $url->getParameter('date');
    if (!$date) $date = $date_parameter;
    $refresh = $url->getParameter("refresh");
    if ($refresh) {
      $min = $url->getParameter("min");
      $max = $url->getParameter("max");
      if (!$pouleid) $pouleid = $min; 
      $next = $pouleid + 1;
      if ($next > $max) $next = $min;
      $html .= '<meta http-equiv="refresh" content="' . $refresh . '; URL=' . SuperGlobals::getme() . "?mm=40&poule=$next&min=$min&max=$max&refresh=$refresh\">";
    } else {
      if (!$date) {
        $html .= '<h1>Kies hier van welke poules je een diashow wilt zien.</h1>';
        $html .= '<a href="' . SuperGlobals::getme() . '?mm=40&min=1&max=4&refresh=10">Vrijdagavond</a></br>';
        $html .= '<a href="' . SuperGlobals::getme() . '?mm=40&min=5&max=11&refresh=10">Zaterdagochtend</a></br>';
        $html .= '<a href="' . SuperGlobals::getme() . '?mm=40&min=12&max=17&refresh=10">Zaterdag tussen de middag</a></br>';
        $html .= '<a href="' . SuperGlobals::getme() . '?mm=40&min=18&max=23&refresh=10">Zaterdagmiddag</a></br>';
        $html .= '<a href="' . SuperGlobals::getme() . '?mm=40&min=24&max=27&refresh=10">Zondagochtend</a></br>';
        $html .= '<a href="' . SuperGlobals::getme() . '?mm=40&min=28&max=30&refresh=10">Zondag tussen de middag</a></br>';
        $html .= '<a href="' . SuperGlobals::getme() . '?mm=40&min=31&max=33&refresh=10">Zondagmiddag</a></br>';
        return $html;
      }
    }
    
    if ($pouleid) {

      //$html = "Poule id = " . $pouleid;

      $poule = $this->tmgr->getPoule($pouleid);
      $html .= $this->getSinglePouleContent($poule);

    } else {

      // All poules. Check date.
      if ($date) {
        $html .= '<h1>Poules op ' . $date . '</h1>';
      } else {
        $html .= '<h1>Alle poules</h1>';
      }
      $poules = $this->tmgr->getPoules($date);
      if (count($poules) == 0) {
        $html .= 'Er spelen geen poules op deze datum.';
      } else {
        foreach ($poules as $poule) {
          $html .= $this->getSinglePouleContent($poule);
        }
      }
    }
    return $html;
	}


  private function getSinglePouleContent($poule)
  {
    $html = '<h2>' . $poule['name'] . '</h2>';
  
    $matches = $this->tmgr->getMatches($poule['id'], false);
    if (count($matches) == 0) {
      $html .= 'Geen wedstrijden in deze poule.';
    } else {
      $html .= '<div class="matches">';
      $html .= '<table>';
      $html .= '<thead>';
      $html .= '<th class="field">Veld</th>';
      $html .= '<th class="time">Tijd</th>';
      $html .= '<th class="match" colspan="2">Wedstrijd</th>';
      $html .= '<th class="score" colspan="2">Uitslag</th>';
      $html .= '</thead>';
      $html .= '<tbody>';
      foreach ($matches as $match) {
        $html .= '<tr>';
        $html .= '<td class="field">' . $match['field'] . '</td>';
        $html .= '<td class="time">' . $match['time'] . '</td>';
        $html .= '<td class="hometeam">' . $match['hometeam'] . '</td>';
        $html .= '<td class="awayteam"> - ' . $match['awayteam'] . '</td>';
        $html .= '<td class="homescore">' . $match['homescore'] . '</td>';
        $html .= '<td class="awayscore"> - ' . $match['awayscore'] . '</td>';
        $html .= '</tr>';
      }
      $html .= '</tbody>';
      $html .= '</table>';
      $html .= '</div>';
    }

    // Show ranking
    $teams = $this->tmgr->getTeams($poule['id']);
    if (count($teams) == 0) {
      $html .= 'Geen teams in deze poule.';
    } else {
      $html .= '<div class="ranking">';
      $html .= '<table>';
      $html .= '<thead>';
      $html .= '<th class="team">Team</th>';
      $html .= '<th class="played">Gespeeld</th>';
      $html .= '<th class="points">Punten</th>';
      $html .= '<th class="win">Gewonnen</th>';
      $html .= '<th class="equal">Gelijk</th>';
      $html .= '<th class="loose">Verloren</th>';
      $html .= '<th class="goals">Voor</th>';
      $html .= '<th class="against">Tegen</th>';
      $html .= '</thead>';
      $html .= '<tbody>';
      foreach ($teams as $team) {
        $html .= '<tr>';
        $html .= '<td class="team">' . $team['name'] . '</td>';
        $html .= '<td class="played">' . $team['played'] . '</td>';
        $html .= '<td class="points">' . $team['points'] . '</td>';
        $html .= '<td class="win">' . $team['win'] . '</td>';
        $html .= '<td class="equal">' . $team['equal'] . '</td>';
        $html .= '<td class="loose">' . $team['loose'] . '</td>';
        $html .= '<td class="goals">' . $team['goals'] . '</td>';
        $html .= '<td class="against"> - ' . $team['against'] . '</td>';
        $html .= '</tr>';
      }
      $html .= '</tbody>';
      $html .= '</table>';
      $html .= '</div>';
    }
    return $html;
  }

  public function getPlayedMatches()
  {
    return $this->tmgr->getPlayedMatches();
  }

  public function getScoredTotal()
  {
    return $this->tmgr->getScoredTotal();
  }

  public function getChampions()
  {
    return $this->tmgr->getChampions();
  }

  public function getTopscorers()
  {
    return $this->tmgr->getTopscorers();
  }

  public function checkPermission()
  {
    return true;
  }

}

?>
