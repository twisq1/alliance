<?php

require_once('php/config.php');
require_once('php/UrlHandler.php');

class Link
{
	protected $icon;
	protected $target;
	protected $parameters;
	protected $attributes;
	protected $name;
	protected $highlight;

	public function __construct($icon, $name) {
		$url = UrlHandler::getInstance();
		$this->icon = IMG_BASEDIR . $icon;
		$this->name = $name;
		$this->highlight = false;
		$this->parameters = $url->getAllParameters();
		$this->attributes = array();
	}
	
	public function setHighlight() {
		$this->highlight = true;
	}
	
	public function setTarget($target) {
		$this->target = $target;
	}
	
	public function setTooltip($tip) {
		$this->setAttribute('title', $tip);
	}
	
	public function getTarget() {
		if ($this->target) {
			return $this->target;
		} else {
			$target = SuperGlobals::getMe();
			if (count($this->parameters)) {
				$target .= '?' . http_build_query($this->parameters);
			}
			return $target;
		}
	}
	
	public function setName($name) {
		$this->name = $name;
	}
	
	public function getName() {
		return $this->name;
	}
	
	private function getIconImage() {
		$html = '<img src="' . $this->icon . '"/>';
		return $html;
	}
	
	public function getIcon() {
		$html = '<a href="' . $this->getTarget() . '">';
		$html .= $this->getIconImage();
		$html .= '</a>';
		return $html;
	}
	
	public function getIconLink() {
		$html = '<a ';
		if ($this->highlight) {
			$html .= 'class="highlight" ';
		}
		$html .= 'href="' . $this->getTarget() . '"' . $this->getAttributesHtml() . '>';
		$html .= $this->getIconImage();
		$html .= '&nbsp;' . $this->name;
		$html .= '</a>';
		return $html;
	}
	
	public function getLink() {
		$string = '<a ';
		if ($this->highlight) {
			$string .= 'class="highlight" ';
		}
		$string .= 'href="' . $this->getTarget() . '"' . $this->getAttributesHtml() . '>' . $this->getName() . '</a>';
		return $string;		
	}

	private function getAttributesHtml()
	{
		$html = '';
		foreach ($this->attributes as $key => $value) {
			$html .= " $key=\"$value\"";	
		}
		return $html;
	}
	
	public function setParameter($name, $value) {
		$this->parameters[$name] = $value;
	}

	public function removeParameter($name) {
		unset($this->parameters[$name]);
	}
	
	public function setAttribute($name, $value) {
		$this->attributes[$name] = $value;
	}
}

?>
