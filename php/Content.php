<?php

require_once('php/config.php');
require_once('php/MainMenu.php');
require_once('php/Login.php');
require_once('php/ContentLogin.php');
require_once('php/ContentDefault.php');
require_once('php/Link.php');

abstract class Content
{
	protected $mainMenu;
	protected $login;
	protected $links;

	public static function InstantiateContent(MainMenu $mainMenu, Login $login, $forceLog = false)
	{
		if (!$mainMenu->isIdValid()) {
			trigger_error('Illegal MainMenuId', E_USER_ERROR);
		}
		$mmid = $mainMenu->searchItem(MMI_LOGIN)->getId();
        if ($forceLog || ($mainMenu->getMainMenuId() == $mmid)) {
			$content = new ContentLogin();
		} else {
			$content = new ContentDefault();
        }
		$content->initialize($mainMenu, $login);
		return $content;
	}

	protected function initialize(MainMenu $mainMenu, Login $login)
	{
		$this->mainMenu = $mainMenu;
		$this->login = $login;
		$this->links = array();
	}

	public abstract function getContent();

	public abstract function checkPermission();
	
}

?>
