<?php

require_once('php/ErrorHandler.php');

abstract class Action
{
	protected $parameters;			// This variable will keep the parameters from the URL.
	protected $mainMenu;
	protected $login;

	public static function InstantiateAction($action, MainMenu $mainMenu, Login $login)
	{
		$classname = 'Action' . $action;
		$classpath = 'php/' . $classname . '.php';
		if (!file_exists($classpath)) {
			trigger_error('Invalid action', E_USER_WARNING);
			return false;
		}
		require_once($classpath);
		$actionClass = new $classname;
		$actionClass->initialize($mainMenu, $login);
		return $actionClass;
	}

	protected function initialize(MainMenu $mainMenu, Login $login)
	{
		$this->mainMenu = $mainMenu;
		$this->login = $login;
		$old_parameters = $_GET;
		$this->parameters = array();
		foreach ($old_parameters as $key => $value) {
			if ($key!='action') {
				$this->parameters[$key] = $value;
			}
		}
	}

	
	/**
	* Each child must check if the userhas the right permissions to do this action.
	**/
	abstract protected function checkPermission();

	/**
	* Each child should then have a function 'execute' which does the action
	**/
	abstract protected function execute();

	/**
	* This function will be called from the main program and is responsible for doing al neccessary steps.
	**/
    public function doAction()
	{
		$this->permission = false;
		if ($this->checkPermission()) {
			$this->execute();
		} else {
			trigger_error('You have no permission to do this action', E_USER_ERROR);
		}
		$this->loadPage();
	}
	

	/**
	* Set Parameter
	* Override a parameter in the URL which will be used by loadPage
	**/
	protected function setParameter($name, $value)
	{
		$this->parameters[$name] = $value;
	}

	protected function removeParameter($name)
	{
		unset($this->parameters[$name]);
	}
	
	/**
	* Call this function at the end of the action to load the next page
	* All parameters in the url are passed, except 'action'.
	**/
	protected function loadPage()
	{
		$url = $_SERVER['PHP_SELF'];
		if (count($this->parameters)) {
			$url .= '?' . http_build_query($this->parameters);
		}

		// Redirect page, but only when there are no errors to display.
		ErrorHandler::redirect($url);
	}
}

?>
