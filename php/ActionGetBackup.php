<?php

require_once('php/config.php');
require_once('php/Action.php');
require_once('php/SuperGlobals.php');
require_once('php/DbFactory.php');
require_once('php/MainMenu.php');
require_once('php/TournamentManager.php');

class ActionGetBackup extends Action
{
	/****
	 * This function is responsible for checking if the user has permissions to do this action
	 *
	 * parameters: none
	 * returns   : true if ok
	 ***/
	protected function checkPermission()
	{
		if ($this->login->getLevel() > 1) {
			return true;
		}
		return false;
	}


	/****
	 * execute - do it!
	 * returns   : nothing
	 ***/
	protected function execute()
	{
        $tmg = new TournamentManager();
        $backup = $tmg->getBackup();

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=tournament.csv');

        // create a file pointer connected to the output stream
        $output = fopen('php://output', 'w');

        // output the column headings
        fputcsv($output, array('poule', 'datum', 'diashow', 'team1', 'team2', 'score1', 'score2', 'veld', 'tijd', 'scheidsrechter'));

        foreach ($backup as $row) {
            fputcsv($output, $row);
        }

        fclose($output);
        exit();
	}
}

?>
