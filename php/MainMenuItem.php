<?php

define('MMI_DEFAULT', 1);
define('MMI_LOGIN', 2);

class MainMenuItem
{
	private $id;				// id which can be placed in URL mm=id
	private $name;				// Name as is shown in menubar
	private $description;		// Tooltip as is shown when cursor hovers over menu item
	private $flags;				// One or more of the defined flags

	function __construct($id, $name, $description, $flags = 0)
	{
		$this->id = $id;
		$this->name = $name;
		$this->description = $description;
		$this->flags = $flags;
	}

	public function setFlag($flag)
	{
		$this->flags |= $flag;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getDescription()
	{
		return $this->description;
	}

	public function checkFlag($flag)
	{
		if ($this->flags & $flag) return true;
		return false;
	}

}

