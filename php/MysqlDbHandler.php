<?php

/****
 * This class implements generic functions for database operations for Mysql databases
 *
 * The interface is defined in dbhandler.php
 *
 ***/

require_once('DbHandler.php');

class MysqlDbHandler implements DbHandler
{
	protected $_db;				// Mysql link id
	protected $_result;			// Result of the last operation
	protected $_host;			// Hostname used at connection
	protected $_name;			// Name of connected database;
	

	/****
	 * The constructor does some generic initialisations
	 *
	 * parameters : none
	 * returns    : nothing
	 * errors     : n/a
	 ***/
	public function __construct()
	{
		$this->_db = false;
	}


	/****
	 * Connect to and select the database
	 *
	 * parameters: host, username, password, database
	 * returns   : false if an error occurs
	 * errors    : are reported to the error handler
	 ***/
	public function connect($host, $username, $password, $database)
	{
		if ($this->_db) {
			trigger_error("Database already connected.", E_USER_WARNING);
			return false;
		}

		$this->_db = mysqli_connect($host, $username, $password, $database);
		if (!$this->_db) {
			trigger_error("Cannot connect to database: " . mysqli_error($this->_db) , E_USER_ERROR);
			return false;
		}
		
		$this->_host = $host;
		$this->_name = $database;
		return true;
	}


	/****
	 * Close database connection if open.
	 *
	 * parameters: none
	 * returns   : false if an error occurs
	 * errors    : reported to the error handler
	 ***/
	public function close()
	{
		if ($this->_result) {
			trigger_error("Last query not freed. Use free_query()", E_USER_WARNING );
		}
		
		if (!$this->_db) {
			trigger_error("Database not connected", E_USER_WARNING);
			return false;
		}

		if (!mysqli_close($this->_db)) {
			trigger_error("Failed to close database connection: " . mysqli_error($this->_db), E_USER_ERROR );
			return false;
		}

		$this->_db = false;
		return true;
	}

	
	/****
	 * Get parameters of the current connection
	 *
	 * parameters: none
	 * returns   : false if not connected
	 *             $make 'mysql' in this case
	 *             $host, host of current connection
	 *             $name, database name of current connection
	 ***/
	public function getConnectionParameters(&$make, &$host, &$name)
	{
		$make = 'mysql';
		$host = $this->_host;
		$name = $this->_name;
		if (!$this->_db) {
			trigger_error("Database not connected", E_USER_NOTICE);
			return false;
		}
		return true;
	}

	
	/****
	 * Fire a query
	 *
	 * parameter: query as an object
	 * returns  : false if an error occurs
	 * error    : are reported to the error handler
	 ***/
	public function executeQuery(DbQuery $query)
	{
		if (!$this->_db) {
			trigger_error("Database not connected", E_USER_ERROR );
			return false;
		}

		if ($this->_result) {
			trigger_error("Previous query not freed. Use free_query()", E_USER_ERROR );
			return false;
		}

		$this->_result = mysqli_query($this->_db, $query->getString());
		if (!$this->_result) {
			$queryString=$query->getString();
			trigger_error("Error (" . mysqli_error($this->_db) . ")while executing query ($queryString)", E_USER_ERROR );
			return false;
		}

		return true;
	}


	/****
	 * Free last query
	 * This function must be called when all results of a query have been inspected
	 * For compatibility reasons this is mandatory. An error occurs when freeing is refrained
	 *
	 * parameters: none
	 * returns   : false on error
	 * errors    : are reported to the error handler
	 ***/
	public function freeQuery()
	{
		if (!$this->_result) {
			trigger_error("No query has been executed successfuly yet", E_USER_WARNING );
			return false;
		}

		if ($this->_result != true) {
			if (!mysqli_free_result($this->_result)) {
				trigger_error("Error while freeing query: " . mysqli_error($this->_db), E_USER_ERROR );
				return false;
			}
		}

		$this->_result = false;
		return true;
	}


	/****
	 * Fetch one row of the last result as an associative array
	 *
	 * parameters: result will be filled with an associative array on success
	 * returns   : true on success
	 * error     : are reported to the error handler
	 ***/
	public function getRecord(&$record)
	{
		if (!$this->_result) {
			trigger_error("No valid result available", E_USER_WARNING );
			return false;
		}

		$record = mysqli_fetch_assoc($this->_result);

		if ($record == NULL) {
			return false;
		}

		return true;
	}
	
	
	/****
	 * Get the number of rows from the last result
	 *
	 * parameters: none
	 * returns   : the number of rows, or false in case of error
	 * errors    : reported to error handler
	 ***/
	public function getNumRows()
	{
		if (!$this->_result) {
			trigger_error("No valid result available", E_USER_WARNING );
			return false;
		}

		if (!$this->_result === true) {
			trigger_error("Last query did not produce a valid result", E_USER_WARNING );
			return false;
		}

		return mysqli_num_rows($this->_result);
	}


	/****
	 * Get the number of rows affected by the last result
	 *
	 * parameters: none
	 * returns   : the number of rows, or false in case of error
	 * errors    : reported to error handler
	 ***/
	public function getAffectedRows()
	{
		if (!$this->_db) {
			trigger_error("Database not connected", E_USER_ERROR );
			return false;
		}

		if (!$this->_result === true) {
			return mysqli_affected_rows($this->_db);
		}
	
		trigger_error("Last query did not produce a valid result", E_USER_WARNING );
		return false;
	}


	/****
	 * Get the id created by autoincrement at the last INSERT query
	 *
	 * parameters: none
	 * returns   : inserted id or false in case of error
	 * errors    : reported to error handler
	 ***/
	public function getInsertedId()
	{
		if (!$this->_db) {
			trigger_error("Database not connected", E_USER_ERROR );
			return false;
		}

		return mysqli_insert_id($this->_db);
	}

	
	/****
	 * When fetching results, use this function to go back to the first result
	 *
	 * parameters: none
	 * returns   : nothing
	 * errors    : reported to error handler
	 ***/
	public function gotoFirstResult()
	{
		if (!$this->_result) {
			trigger_error("No valid result available", E_USER_WARNING );
			return false;
		}

		if (!$this->_result === true) {
			trigger_error("Last query did not produce a valid result", E_USER_WARNING );
			return false;
		}

		if (!mysqli_data_seek($this->_result, 0)) {
			trigger_error("Error at data seek: " . mysqli_error($this->_db), E_USER_ERROR);
			return false;
		}

		return true;
	}

}

?>
