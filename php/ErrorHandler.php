<?php 
/****
 * Error Handler
 *
 * This error handler can be used to report errors during a script.
 * Warnings and notices will be stored in an array. These can be read later.
 * Errors will be stored in the array as well, but they will also throw an exception with an ErrorHandlerException object.
 * The main script is responsible for catching this exception
 *
 * Use trigger_error($message, $type) to report a message to this error handler.
 * $type can be:
 *   E_USER_ERROR   - if a fatal error occurs and the script can not continue, an exception will be thrown
 *   E_USER_WARNING - if a non fatal error occurs. The message will be recorded and the script will continue
 *   E_USER_NOTICE  - if something happens which is no disaster but should be noticed to the user
 *
 ***/

//require_once('php/Manager/ErrorManager.php');
 
/****
 * The class ErrorHandlerException is created just to distinguish from other exceptions
 ***/
class ErrorHandlerException extends Exception
{
}


/****
 * The ErrorHandler class is used to redirect the standard error handler.
 * All messages will be stored in an array, so they can be reported later.
 * Errors will throw an exception
 ***/
class ErrorHandler
{
	private static $_singleton = null;		// The one and only error handler object
	protected $_errorList;					// This array contains all reported messages
	protected $_unreportedErrors;			// True if the errorList contains errors which are not reported yet
	

	/****
	 * Initialize this class and redirect the error handler
	 ***/
	private function __construct()
	{
		$this->_errorList = array();
		$this->_unreportedErrors = false;
		set_error_handler(array($this, "callBack"));
	}


	/****
	 * If not all errors are reported at the end of life, the destructor will report them
	 ***/
	public function __destruct()
	{
		if ($this->_unreportedErrors) {
			$this->printAllErrors();
		}
	}
	

	/****
	 * This is the callBack function which will be called if anywhere in the script an error occurs.
	 * Normally these errors will be triggered by trigger_error()
	 * This functions adds the error info to the errorList and throws an exception if the type
	 * of error is error (and not warning or notice)
	 ***/
	public function callBack($errno, $errstr, $errfile, $errline)
	{
		$errorData = array(
			'type' => $errno,
			'message' => $errstr,
			'file' => $errfile,
			'line' => $errline
		);
		
		array_push($this->_errorList, $errorData);
		$this->_unreportedErrors = true;

		switch($errno) {
			case E_ERROR:
			case E_PARSE:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
				throw new ErrorHandlerException("Error: $errstr");
		}
		return true;
	}
	
	
	/****
	 * returns a string with the type of error
	 ***/
	protected function getTypeString($errorType)
	{
		switch($errorType)
		{
			case E_ERROR:
			case E_CORE_ERROR:
			case E_COMPILE_ERROR:
			case E_USER_ERROR:
			case E_PARSE:
				$errorTypeString = 'error';
				break;
			case E_WARNING:
			case E_CORE_WARNING:
			case E_COMPILE_WARNING:
			case E_USER_WARNING:
				$errorTypeString = 'warning';
				break;
			case E_NOTICE:
			case E_USER_NOTICE:
				$errorTypeString = 'notice';
				break;
			default:
				$errorTypeString = 'unknown';
				break;
		}
		return $errorTypeString;
	}
	
	
	/****
	 * printError prints one error message
	 *
	 * parameters: $errorData; the data of the error in an associative array
	 *             $outputType; if this is HTML, the error will be printed in HTML
	 * returns   : nothing
	 ***/
	protected function printError($errorData, $outputType)
	{
		$errorType    = $errorData['type'];
		$errorMessage = $errorData['message'];
		$errorFile    = $errorData['file'];
		$errorLine    = $errorData['line'];

		$errorTypeString = $this->getTypeString($errorType);
		
		if ($outputType == 'HTML') {
			print('<P><PRE>');
			print('*** ERROR ***<BR/>');
			print("type   : $errorTypeString<BR/>");
			print("file   : $errorFile<BR/>");
			print("line   : $errorLine<BR/>");
			print("message: $errorMessage");
			print('</PRE></P>');
		} else {
			print("\n*** ERROR ***\ntype   : $errorTypeString\nfile   : $errorFile\nline   : $errorLine\nmessage: $errorMessage.\n************************\n");
		}
	}
	

	/****
	* Store errors
	* store errors to database if possible, otherwise print them
	* returns an context_id of the reported errors.
	****/
	function storeErrors($outputType)
	{
//		$errorManager = new ErrorManager();
//		$errorManager->open();
//		$errorContextId = 0;
//		foreach ($this->_errorList as $errorData) {
//			if (!$errorContextId) {
//				$errorContextId = $errorManager->createContext();
//			}
//			$errorType    = $this->getTypeString($errorData['type']);
//			$errorMessage = $errorData['message'];
//			$errorFile    = $errorData['file'];
//			$errorLine    = $errorData['line'];
//			if (!$errorManager->addMessage($errorContextId, $errorType, $errorFile, $errorLine, $errorMessage)) {
//				$this->printError($errorData, $outputType);
//			}
//		}
//		$errorManager->close();
//		$this->_unreportedErrors = false;
//		return $errorContextId;
	}
	
	/****
	 * print All Errors
	 * report all collected error (and warning and notice) messages
	 *
	 * parameters: $outputType; if this is HTML, messages will be printed in HTML format
	 * returns   : nothing
	 ***/
	public function printAllErrors($outputType='NORMAL')
	{
		foreach ($this->_errorList as $errorData) {
			$this->printError($errorData, $outputType);
		}
		$this->_unreportedErrors = false;
	}
	
	
	/****
	 * initialize
	 *
	 * An error handler object can only be instantiated with this method. Thanks to the singleton pattern
	 * only one object can exist. 
	 ***/
	static public function init()
	{
		if (!ErrorHandler::$_singleton) {
			ErrorHandler::$_singleton = new ErrorHandler;
		}
		return ErrorHandler::$_singleton;
	}


	/****
	 * Restore default error handler
	 * use this function for example after catching an error
	 ***/
	public function restoreDefaultHandler()
	{
		restore_error_handler();
	}
	
	
	/***
	 * return true if one or more messages have been reported
	 **/
	public function hasMessages()
	{
		return $this->_unreportedErrors;
	}

	/***
	 * redirect
	 * sends HTML headers, but only when there are no error messages to display
	 **/
	static public function redirect($url)
	{
		$errorHandler = ErrorHandler::init();
		if (!$errorHandler->hasMessages()) {
			header("Location: $url");
		}
	}
	
}

?>
