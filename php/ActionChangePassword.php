<?php

require_once('php/config.php');
require_once('php/Action.php');
require_once('php/SuperGlobals.php');
require_once('php/DbFactory.php');
require_once('php/MainMenu.php');

class ActionChangePassword extends Action
{
	/****
	 * This function is responsible for checking if the user has permissions to do this action
	 *
	 * parameters: none
	 * returns   : true if ok
	 ***/
	protected function checkPermission()
	{
		if ($this->login->changePasswordAllowed()) {
			return true;
		}
		return false;
	}


	/****
	 * execute - do it!
	 * returns   : nothing
	 ***/
	protected function execute()
	{
		$oldpw = SuperGlobals::getPost('old');
		$new1 = SuperGLobals::getPost('password1');
		$new2 = SuperGlobals::getPost('password2');
		
		// First check old password
		if (!$this->login->checkPassword(md5($oldpw))) {
			$this->setParameter('result','wrong');
		} else {
			
			// Check if new passwords are the same
			if ($new1 != $new2) {
				$this->setParameter('result','different');
			} else {
				
				// Change password
				$md5 = md5($new1);
				$dbh = DbFactory::getDefaultHandler();
				$query = new DbQuery("UPDATE user SET `md5` = ':md5' WHERE user_id = ':user_id'");
				$query->bindParam('user_id',$this->login->getUserId());
				$query->bindParam('md5', $md5);
				$dbh->executeQuery($query);
				$this->setParameter('result','ok');
				$dbh->freeQuery();
        		SuperGlobals::setSession('md5', $md5);
			}
		}
	}
}

?>
