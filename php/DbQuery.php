<?php

/****
 * This class can be used to prepare an SQL query and pass it to an instance of the database handler.
 *
 * Internal errors are reported to the default error handler and can be trapped by your user error handler.
 *
 * This class contains these functions
 *
 *	public function prepare($query);					// Start a new query with this template string
 *	public function bindParam($param, $value);			// Fill in value for parameter. The parameter can be overwritten unlimited.
 *	public function getString();						// Return query as a composed string.
 *
 ***/

 
/****
 * The class implements the functions which are defined in the interface
 ***/
class DbQuery
{
	protected $_template;
	protected $_parameters;

	/****
	 * The constructor sets $_template to a valid string value and initialized $_parameters as an empty array
	 ***/
	public function __construct($template = '')
	{
		$this->_template = $template;
		$this->_parameters = array();
	}


	/****
	 * Use prepare() to set a template string of the query. Use :name where a parameter is expected.
	 * These parametes can be filled in later with bindParam()
	 *
	 * parameters: the template string
	 * returns   : true
	 ***/
	public function prepare($query)
	{
		$this->_template = $query;
	}

	/****
	 * Use bind parameter to fill in a parameter of the previously defined template query string
	 *
	 * parameters: - the name of the parameter without the colon
	 *             - the value to be filled in
	 * returns   : true if the parameter can be substituted
	 * errors    : n/a
	 ***/
	public function bindParam($param, $value)
	{
		$this->_parameters[$param] = $value;
		return true;
	}

	/****
	 * Use this function to retrieve the query as a string value
	 *
	 * parameters: none
	 * returns   : the query string
	 * errors    : reported to the error handler
	 ***/
	public function getString()
	{
		$querystring = $this->_template;

		// Search for parameters in template string and replace them by their values
		foreach ($this->_parameters as $key => $value) {
			$needle = ":$key";
			if (strpos($querystring, $needle) === false) {
				trigger_error("Parameter $needle not found", E_USER_WARNING);
			}
		
			$replace = addslashes($value);
			$filledIn = str_replace($needle, $replace, $querystring);
			$querystring = $filledIn;
		}

		return $querystring;
	}
	
}

?>
