<?php
session_start();

require_once('php/config.php');
require_once('php/MainMenu.php');
require_once('php/Content.php');
require_once('php/Login.php');
require_once('php/Action.php');
require_once('php/DbFactory.php');
require_once('php/UrlHandler.php');

// Manage URL
$url = UrlHandler::getInstance();
$url->checkContent();

// Instantiate global classes and check login.
$login = new Login();
$login->check();
$mainmenu = new MainMenu($mainMenuDefinition);

// Check if there is an action to perform.
$actionName = $url->getParameter('action');
if ($actionName) {
	$action = Action::instantiateAction($actionName, $mainmenu, $login);
	if ($action) {
		$action->doAction();
	}
}

?>

<!DOCTYPE html>
<html lang="nl">
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

	<head>
		<title>KiKa Alliance'22 Toernooi Manager</title>
		<link rel="stylesheet" type="text/css" href="css/main.css">
		<script>
			/* When the user clicks on the button, 
			toggle between hiding and showing the dropdown content */
			function toggleMenu() {
				document.getElementById("minimenu").classList.toggle("show");
			}

			// Close the dropdown if the user clicks outside of it
			window.onclick = function(e) {
				if (!e.target.matches('.dropbtn')) {
				var myDropdown = document.getElementById("minimenu");
					if (myDropdown.classList.contains('show')) {
					myDropdown.classList.remove('show');
					}
				}
			}
		</script>
	</head>

	<body>

		<header>
			<div class="logo col-2">
				<img class="logo-img" src="<?php print(IMG_BASEDIR . IMG_LOGO); ?>"/>
			
			<!-- Do not put whitespace between these DIVs! -->
			</div><div class="title col-8">
				<img class="dropbtn" onclick="toggleMenu()" src="img/bars-solid.svg"/>
				KiKa ALLIANCE &#39;22 JEUGDTOERNOOI

			<!-- Do not put whitespace between these DIVs! -->
			</div><div class="login col-2">
				<?php
					if ($login->getLevel()) print($login->getUsername());
				?>
			</div>
		</header>

		<nav class="col-2" id="mainmenu">
			<?php
				$mainmenu->show(false);
			?>

		<!-- Do not put whitespace between NAV and DIV! -->
		</nav><div class="dropper" id="minimenu">
			<a class="minioption" href='?mm=10'>VANDAAG</a>
			<a class="minioption" href='?mm=12'>VRIJDAG</a>
			<a class="minioption" href='?mm=15'>ZATERDAG</a>
			<a class="minioption" href='?mm=20'>ZONDAG</a>
			<a class="minioption" href='?mm=30'>ALLE WEDSTRIJDEN</a>
			<a class="minioption" href='?mm=100'>LOGIN</a>
		</div><div class="content col-8">

			<?php
			$html = '';
			$content = Content::instantiateContent($mainmenu, $login);
			if ($content->checkPermission()) {
				$html .= $content->getContent();
			} else {
				if ($login->getUserId() == false) {
					$loginContent = Content::instantiateContent($mainmenu, $login, true);
					$loginContent->setPage('log');
					$html .= $loginContent->getContent();
				} else {
					$html .= 'Je hebt geen toegang tot deze pagina.';
				}
			}
			print($html);
			?>
		</div><div class="champions col-2">
			<?php
				$html = '';
				$content = Content::instantiateContent($mainmenu, $login);
				$played = $content->getPlayedMatches();
				$scored = $content->getScoredTotal();
				$champions = $content->getTopscorers();
				$html .= '<h1>Toernooi</h1>';
				$html .= "$played wedstrijden gespeeld<br/>";
				$html .= "$scored doelpunten gescoord<br/>";
				if (count($champions) > 0) {
					$html .= '<h1>Topscorers</h1>';
					$html .= '<table><tbody>';
					foreach($champions as $champion) {
						$html .= '<tr><td><a href="?mm=10&poule=' . $champion['poule_id'] . '">' . $champion['name'] . '</a></td><td>' . $champion['goals'] . '</td></tr>';
					}
					$html .= '</tbody></table>';
				}
				print($html);
			?>
		</div>

		<footer>
			<a id="twisq" title="De maker van deze site" href="https://www.twisq.nl">www.twisq.nl</a>
		</footer>

	</body>
</html>
<?php DbFactory::closeDatabase(); ?>
